package fr.cea.kemanager.dpos.key.vault;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.TransitKeyType;
import org.springframework.vault.support.VaultTransitKey;
import org.springframework.vault.support.VaultTransitKeyCreationRequest;

/**
 * This class encapsulates the operations for AES key management from Vault.
 *
 * @author Quentin Lutz
 */
public class AESKeyManager {

	private VaultTransitTemplate m_temTransit;

	/**
	 * Create a new {@link AESKeyManager} with a {@link VaultTransitTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public AESKeyManager(VaultTransitTemplate transit) {
		this.m_temTransit = transit;
	}

	/**
	 * Create a new AES key.
	 *
	 * @param userID is the ID for the active user.
	 */
	public void CreateAESPair(String userID) throws NoSuchAlgorithmException, InvalidKeySpecException {
		if (m_temTransit.getKey("AES"+ userID)==null) {
			VaultTransitKeyCreationRequest keySpecs = VaultTransitKeyCreationRequest.builder()
					.type("aes256-gcm96")
					.derived(false)
					.exportable(true)
					.convergentEncryption(false)
					.build();

			m_temTransit.createKey("AES"+ userID,keySpecs);
		} else {
			m_temTransit.rotate("AES"+ userID);
		}
	}


	/**
	 * Return the values of the secret key
	 * of a given AES key pair.
	 *
	 * @param userID is the ID for the active user.
	 */
	public byte[] GetAESSecretKey(String userID) {
		if (m_temTransit.getKey("AES"+ userID) == null) {
			try {
				CreateAESPair(userID);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		VaultTransitKey keyAES = m_temTransit.getKey("AES"+ userID);
		
		int minVersion = keyAES.getLatestVersion();

		return m_temTransit.exportKey("AES"+ userID, TransitKeyType.ENCRYPTION_KEY)
				.getKeys()
				.get(String.valueOf(minVersion))
				.getBytes();
		
	}
}
