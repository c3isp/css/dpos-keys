package fr.cea.kemanager.dpos.key.restapi;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.dpos.key.vault.AESKeyManager;
import fr.cea.kemanager.key.models.KeyObject;
import fr.cea.kemanager.vault.common.VaultStart;
import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T10:57:36.575Z")

@RestController
@RequestMapping("/v1")
public class KeyApiController implements KeyApi {

	private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    
    @Value("${token.c3isp.cnr.it}")
    String TokenRoot;

    @org.springframework.beans.factory.annotation.Autowired
    public KeyApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }
    
    

    @Override 
    public ResponseEntity<String> createKey(
    		@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                	log.info("ResponseEntity<ServerResponse> createKey method start working");
                	AESKeyManager aESKeyManager = new AESKeyManager(new VaultStart(TokenRoot).GetTransit());
                	aESKeyManager.CreateAESPair(dsAId);
                	String message = "DPOS key pair created for DSA Id "+ dsAId; 
                	return new ResponseEntity<String>(message, HttpStatus.OK);
                } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                	log.error("Couldn't create keys for the given parameters", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeyApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @Override 
    public ResponseEntity<List<KeyObject>> getKey(
    		@ApiParam(value = "",required=true) @PathVariable("requestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                log.info("ResponseEntity<List<KeyObject>> getKey method start working");
				List<KeyObject> lstKeys = new ArrayList<KeyObject>();
				AESKeyManager aESKeyManager = new AESKeyManager(new VaultStart(TokenRoot).GetTransit());
				byte[] key = aESKeyManager.GetAESSecretKey(dsAId);
				
				lstKeys.add((new KeyObject()).key(key)
						.keyInfo(KeyObject.KeyInfoEnum.DPOS_SK)
						 .requestId(requestId)
						 .checksum(getChecksumFromByte(key)));
				return new ResponseEntity<List<KeyObject>>(lstKeys, HttpStatus.OK);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default KeyApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    private String getChecksumFromByte(byte[] ori) {
    	MessageDigest md=null;
		try {
			md = MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	md.update(ori, 0, ori.length);
    	byte[] mdbytes = md.digest();
    	StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < mdbytes.length; i++) {
        	sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
    	return sb.toString();
    }
}
