package fr.cea.kemanager.dpos.key.vault;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.TransitKeyType;
import org.springframework.vault.support.VaultTransitKey;
import org.springframework.vault.support.VaultTransitKeyCreationRequest;



/**
 * This class encapsulates the operations for RSA key management from Vault.
 *
 * @author Quentin Lutz
 */
public class RSAKeyManager {

	private VaultTransitTemplate m_temTransit;

	/**
	 * Create a new {@link RSAKeyManager} with a {@link VaultTransitTemplate}.
	 *
	 * @param transit must not be {@literal null}.
	 */
	public RSAKeyManager(VaultTransitTemplate transit) {
		this.m_temTransit = transit;
	}

	/**
	 * Create a new RSA key pair.
	 *
	 * @param userID is the ID for the active user.
	 */
	public void CreateRSAPair(String userID) throws NoSuchAlgorithmException, InvalidKeySpecException {
		if (m_temTransit.getKey("RSA"+ userID)==null) {
			VaultTransitKeyCreationRequest keySpecs = VaultTransitKeyCreationRequest.builder()
					.type("rsa-4096")
					.derived(false)
					.exportable(true)
					.convergentEncryption(false)
					.build();

			m_temTransit.createKey("RSA"+ userID,keySpecs);
		} else {
			m_temTransit.rotate("RSA"+ userID);
		}
	}

	/**
	 * Return the values of a given RSA key pair.
	 * The public key and the private key are returned in that order.
	 *
	 * @param userID is the ID for the active user.
	 */
	public ArrayList<byte[]> GetRSAPair(String userID) {
		ArrayList<byte[]> keys = new ArrayList<byte[]>();
		VaultTransitKey keyRSA = m_temTransit.getKey("RSA"+ userID);
		if (keyRSA == null) {
			try {
				CreateRSAPair(userID);
				keyRSA = m_temTransit.getKey("RSA"+ userID);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int minVersion = keyRSA.getLatestVersion();
		keys.add(((String) ((LinkedHashMap<?, ?>) keyRSA
				.getKeys()
  				.get(String.valueOf(minVersion)))
				.get("public_key"))
				.getBytes());

		keys.add(m_temTransit.exportKey("RSA"+ userID, TransitKeyType.ENCRYPTION_KEY)
				.getKeys()
				.get(String.valueOf(minVersion))
				.getBytes());
		
		return keys;
	}
	
	/**
	 * Return the values of the public key
	 * of a given RSA key pair.
	 *
	 * @param userID is the ID for the active user.
	 */
	public byte[] GetRSAPublicKey(String userID) {
		VaultTransitKey keyRSA = m_temTransit.getKey("RSA"+ userID);
		int minVersion = keyRSA.getLatestVersion();
		return ((String) ((LinkedHashMap<?, ?>) keyRSA
				.getKeys()
  				.get(String.valueOf(minVersion)))
				.get("public_key"))
				.getBytes();
	}
	
	/**
	 * Return the values of the secret key
	 * of a given RSA key pair.
	 *
	 * @param userID is the ID for the active user.
	 */
	public byte[] GetRSASecretKey(String userID) {
		VaultTransitKey keyRSA = m_temTransit.getKey("RSA"+ userID);
		int minVersion = keyRSA.getLatestVersion();

		return m_temTransit.exportKey("RSA"+ userID, TransitKeyType.ENCRYPTION_KEY)
				.getKeys()
				.get(String.valueOf(minVersion))
				.getBytes();
		
	}
	
}
